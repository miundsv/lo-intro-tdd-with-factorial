#include "catch.hpp"
#include "factorial.h"

TEST_CASE("factorial(0) is the base case") {
  REQUIRE(factorial(0) == 1);
}

/*
 * This test case is not strictly necessary, since it should be subsumed by
 * the "for all n > 0" case. However, that relies on some slight cleverness,
 * and I would feel more confident knowing that there is at least one specific
 * case. So whether to include this is mostly a matter of taste.
 */
TEST_CASE("factorial(5) relies on several recursive calls") {
  REQUIRE(factorial(5) == 5*4*3*2*1);
}

TEST_CASE("for n > 0: factorial(n) = n*factorial(n-1)") {
  int n = rand() % 12 + 1;  // n in [1,12]
  REQUIRE(factorial(n) == n*factorial(n-1));
}

TEST_CASE("for all n: factorial(n) >= 1") {
  int n = rand() % 12 + 1;  // n in [1,12]
  REQUIRE(factorial(n) >= 1);
}

/*
 * The function should arguably take an unsigned int, which would make this
 * case unnecessary.
 */
TEST_CASE("n < 0 should throw error") {
  REQUIRE_THROWS(factorial(-1));
}

TEST_CASE("n > 12 should throw error") {
  REQUIRE_THROWS(factorial(13));
}
