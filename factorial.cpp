#include "factorial.h"
#include "stdexcept"

int factorial(int n) {
  if (n < 0 || n > 12) {
    throw std::domain_error("n must be >= 0");
  }
  return (n == 0) ?
    1 :
    n * factorial(n-1);
}
