#include "factorial.h"
#include <cassert>

int main() {
  assert(factorial(0) == 1);
  //assert(factorial(3) == 3);  // Expected to fail.
  assert(factorial(2) == 2);

  return 0;
}
